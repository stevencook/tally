@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default" ng-controller="talliesController" class="ng-cloak">
			<div class="panel-heading">Events (<% numberOfEvents %>)</div>

			<div class="panel-body">
				<div>

					<!-- Table-to-load-the-data Part -->
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Start</th>
								<th>End</th>
								<th>Description</th>
								<th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Event</button></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="tally in tallies">
								<td><% tally.name %></td>
								<td><% tally.starts | date : 'MMM dd, yyyy h:mma' %></td>
								<td><% tally.ends | date : 'MMM dd, yyyy h:mma' %></td>
								<td><% tally.description %></td>
								<td>
									<a class="btn btn-default btn-xs btn-detail" href="/event/<% tally.id %>/<% tally.token %>/{{ $email }}">View</a>
									<button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', tally.id)">Edit</button>
									<button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(tally.id)">Delete</button>
								</td>
							</tr>
						</tbody>
					</table>
					<!-- End of Table-to-load-the-data Part -->

					<!-- Modal (Pop up when detail button clicked) -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
									<h4 class="modal-title" id="myModalLabel"><%form_title%></h4>
								</div>
								<div class="modal-body">
									<form name="frmtallies" class="form-horizontal" novalidate="">

										<div class="form-group error">
											<label for="name" class="col-sm-3 control-label">Name</label>
											<div class="col-sm-9">
												<input type="text" class="form-control has-error" id="name" name="name" placeholder="Name" value="<%name%>" ng-model="tally.name" ng-required="true">
												<span class="help-inline" ng-show="frmtallies.name.$invalid && frmtallies.name.$touched">Name field is required</span>
											</div>
										</div>

										<div class="form-group error">
											<label for="starts" class="col-sm-3 control-label">Starts</label>
											<div class="col-sm-9">
												<p class="input-group">
													<input type="text" class="form-control has-error" id="starts" name="starts" placeholder="Starts" value="<%starts%>" ng-model="tally.starts" ng-required="true" datetime-picker="MMM dd, yyyy h:mma" is-open="tally.startsDatePickerIsOpen">
													<span class="input-group-btn">
														<button type="button" class="btn btn-default" ng-click="valuationDatePickerOpen($event, 'starts')"><i class="fa fa-calendar"></i></button>
													</span>
													<span class="help-inline" ng-show="frmtallies.starts.$invalid && frmtallies.starts.$touched">Start time is required</span>
												</p>
											</div>
										</div>

										<div class="form-group error">
											<label for="ends" class="col-sm-3 control-label">Ends</label>
											<div class="col-sm-9">
												<p class="input-group">
													<input type="text" class="form-control has-error" id="ends" name="ends" placeholder="Ends" value="<%ends%>" ng-model="tally.ends" ng-required="true" datetime-picker="MMM dd, yyyy h:mma" is-open="tally.endsDatePickerIsOpen">
													<span class="input-group-btn">
														<button type="button" class="btn btn-default" ng-click="valuationDatePickerOpen($event, 'ends')"><i class="fa fa-calendar"></i></button>
													</span>
													<span class="help-inline" ng-show="frmtallies.ends.$invalid && frmtallies.ends.$touched">End time is required</span>
												</p>
											</div>
										</div>

										<div class="form-group error">
											<label for="description" class="col-sm-3 control-label">Description</label>
											<div class="col-sm-9">
												<textarea type="text" class="form-control has-error" id="description" name="description" placeholder="Description" ng-model="tally.description" ng-required="true"><%description%></textarea>
												<span class="help-inline" ng-show="frmtallies.description.$invalid && frmtallies.description.$touched">Description field is required</span>
											</div>
										</div>

									</form>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmtallies.$invalid">Save changes</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div ng-controller="MyController as mc">
	<p class="input-group">
		<input type="text" class="form-control" datetime-picker="MMM dd, yyyy h:mma" ng-model="myDate" is-open="mc.valuationDatePickerIsOpen"  />
		<span class="input-group-btn">
			<button type="button" class="btn btn-default" ng-click="mc.valuationDatePickerOpen($event, prop)"><i class="fa fa-calendar"></i></button>
		</span>
	</p>
</div> -->
@endsection

@section('header')
	<script src="{{ asset('app/controllers/tallies.js') }}"></script>
@endsection
