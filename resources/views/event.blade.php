@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{ $email }} is invited to: {{ $event->name }}</div>

				<div class="panel-body">
					<h4>{{ $dateString }}</h4>
					<p>{!! nl2br($event->description) !!}</p>
					@if ($canRSVP)
						<h4>Will you be attending?</h4>
						<a href="{{ action('TallyController@rsvp', [$event->id, $event->token, 1, $email]) }}" class="btn btn-default">YES</a>
						<a href="{{ action('TallyController@rsvp', [$event->id, $event->token, 0, $email]) }}" class="btn btn-default">NO</a>
					@else
						<h4>Thank you for your RSVP</h4>
					@endif
					@if (count($attending))
						<h4>Attending ({{ count($attending) }}):</h4>
						<ul>
							@foreach ($attending as $guest)
								<li>{{ $guest->email }}</li>
							@endforeach
						</ul>
					@endif
					@if (count($notAttending))
						<h4>Not Attending ({{ count($notAttending) }}):</h4>
						<ul>
							@foreach ($notAttending as $guest)
								<li>{{ $guest->email }}</li>
							@endforeach
						</ul>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
