<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tally extends Model
{
	// The tally belongs to one user
	public function position() {
		return $this->belongsTo('App\User');
	}

	// The tally has many attendees
	public function attendees() {
		return $this->hasMany('App\Attendee');
	}
}
