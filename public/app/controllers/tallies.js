app.controller('talliesController', function($scope, $http, API_URL) {

	var endpoint = 'tally';

	// Retrieve tallies listing from API
	$http.get(API_URL + endpoint).success(function(tallies) {

		// Format date from DB as angular date
		for (var i=0; i<tallies.length; i++) {
			tallies[i].starts = tallies[i].starts.replace(' ', 'T');
			tallies[i].starts = Date.parse(tallies[i].starts);
			tallies[i].ends = tallies[i].ends.replace(' ', 'T');
			tallies[i].ends = Date.parse(tallies[i].ends);
		}

		// Bind data
		$scope.tallies = tallies;
		$scope.numberOfEvents = tallies.length;

		// Set up adding new dates
		$scope.tally = {};
		$scope.tally.starts = new Date();
		$scope.tally.ends = new Date();

		$scope.tally.startsDatePickerIsOpen = false;
		$scope.tally.endsDatePickerIsOpen = false;
	});

	// Show modal form
	$scope.toggle = function(modalstate, id) {
		$scope.modalstate = modalstate;

		switch (modalstate) {
			case 'add':
				$scope.form_title = "Add New Event";
				$scope.tally.starts = new Date();
				$scope.tally.ends = new Date();
				$scope.tally.startsDatePickerIsOpen = false;
				$scope.tally.endsDatePickerIsOpen = false;
				break;
			case 'edit':
				$scope.form_title = "Edit Event";
				$scope.id = id;
				$http.get(API_URL + endpoint + '/' + id)
					.success(function(response) {
						console.log(response);

						// Format date from DB as angular date
						response.starts = response.starts.replace(' ', 'T');
						response.starts = Date.parse(response.starts);
						response.ends = response.ends.replace(' ', 'T');
						response.ends = Date.parse(response.ends);

						response.startsDatePickerIsOpen = false;
						response.endsDatePickerIsOpen = false;

						// Assign to page
						$scope.tally = response;
					});
				break;
			default:
				break;
		}
		console.log(id);
		$('#myModal').modal('show');
	}

	// Save new record / update existing record
	$scope.save = function(modalstate, id) {
		var url = API_URL + endpoint;
		var data = $.param($scope.tally);

		// Append tally id to the URL if the form is in edit mode
		if (modalstate === 'edit'){
			url += "/" + id;
			// Add PUT to post variables so Laravel detects is as an update
			data += '&_method=PUT';
		}

		$http({
			method: 'POST',
			url: url,
			data: data,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).success(function(response) {
			console.log(response);
			location.reload();
		}).error(function(response) {
			console.log(response);
			alert('This is embarassing. An error has occured. Please check the log for details');
		});
	}

	// Delete record
	$scope.confirmDelete = function(id) {
		var isConfirmDelete = confirm('Are you sure you want to delete this event?');
		if (isConfirmDelete) {
			$http({
				method: 'DELETE',
				url: API_URL + endpoint + '/' + id
			}).
				success(function(data) {
					console.log(data);
					location.reload();
				}).
				error(function(data) {
					console.log(data);
					alert('Unable to delete');
				});
		} else {
			return false;
		}
	}

	$scope.valuationDatePickerOpen = function(e, field) {
		e.preventDefault();
		e.stopPropagation();

		if (field == 'starts') {
			$scope.tally.startsDatePickerIsOpen = true;
			$scope.tally.endsDatePickerIsOpen = false;
			if (!$scope.tally.starts) {
				$scope.tally.starts = new Date();
			}
		} else if (field == 'ends') {
			$scope.tally.endsDatePickerIsOpen = true;
			$scope.tally.startsDatePickerIsOpen = false;
			if (!$scope.tally.ends) {
				$scope.tally.ends = new Date();
			}
		}
	}

});



// app.controller('MyController', function($scope) {
//     var mc = this;

// 	$scope.myDate = new Date();

//     mc.valuationDatePickerIsOpen = false;

//     mc.valuationDatePickerOpen = function(e) {
//     	console.log('open');
//         e.preventDefault();
//         e.stopPropagation();

//         if (!$scope.myDate) {
//         	$scope.myDate = new Date();
//         }

//         mc.valuationDatePickerIsOpen = true;
//     }
// });