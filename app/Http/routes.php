<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {

	/* Home page */
	Route::get('/', function () {
		return redirect('/dashboard');
	});

	/* Allow people to log in */
	Route::Auth();

	/* RSVP pages */
	Route::get('event/{id}/{token}/{email?}', 'TallyController@eventLanding');
	Route::get('rsvp/{id}/{token}/{choice}/{email?}', 'TallyController@rsvp');

	/* Authenticated routes */
	Route::group(['middleware' => 'auth'], function() {
		Route::group(['prefix' => 'dashboard'], function() {
			Route::get('/', 'HomeController@index');
		});
	});

	/* API: middleware covers throttling and authentication */
	Route::group(['middleware' => 'api'], function() {
		Route::group(['prefix' => 'api/v1'], function() {
			Route::resource('tally', 'TallyController');
		});
	});

});

