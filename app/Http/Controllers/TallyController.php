<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tally as Tally;
use App\Attendee as Attendee;

use Carbon\Carbon;

class TallyController extends Controller {

	private function validateTally($request) {

		// Validate the input
		$this->validate($request, [
			'name' => 'required',
			'starts' => 'required',
		]);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Tally::all();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		self::validateTally($request);

		$starts_array = date_parse($request->input('starts'));
		$ends_array = date_parse($request->input('ends'));

		// Returns original date string assuming the format was Y-m-d H:i:s
		$starts = date('Y-m-d H:i:s', mktime($starts_array['hour'], $starts_array['minute'], $starts_array['second'], $starts_array['month'], $starts_array['day'], $starts_array['year']));
		$ends = date('Y-m-d H:i:s', mktime($ends_array['hour'], $ends_array['minute'], $ends_array['second'], $ends_array['month'], $ends_array['day'], $ends_array['year']));

		$tally = new Tally;
		$tally->name = $request->input('name');
		$tally->starts = $starts;
		$tally->ends = $ends;
		$tally->description = $request->input('description');
		$tally->token = str_random(16);
		$tally->save();

		return 'Tally created';
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Tally::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		self::validateTally($request);

		$tally = Tally::find($id);
		$tally->name = $request->input('name');

		// Angular DatePicker passes as a number instead of string
		// if the date is staged to be edited but not clicked on
		// Fix: skip numeric updates
		if (!is_numeric($request->input('starts'))) {
			$starts_array = date_parse($request->input('starts'));
			$starts = date('Y-m-d H:i:s', mktime($starts_array['hour'], $starts_array['minute'], $starts_array['second'], $starts_array['month'], $starts_array['day'], $starts_array['year']));
			$tally->starts = $starts;
		}
		if (!is_numeric($request->input('ends'))) {
			$ends_array = date_parse($request->input('ends'));
			$ends = date('Y-m-d H:i:s', mktime($ends_array['hour'], $ends_array['minute'], $ends_array['second'], $ends_array['month'], $ends_array['day'], $ends_array['year']));
			$tally->ends = $ends;
		}

		$tally->description = $request->input('description');
		$tally->save();

		return 'Tally updated';
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tally = Tally::find($id);
		$tally->delete();
		return 'Tally deleted';
	}

	/**
	 * Display the event details
	 *
	 * @param  int  $id
	 * @param  string  $token
	 * @return Response
	 */
	public function eventLanding($id, $token, $email = null)
	{
		try {

			// Load the tally
			$tally = Tally::find($id);
			if ($tally->token != $token) {
				throw new \Exception('Token mismatch');
			}

			$guests = $tally->attendees;

			// Don't allow people who have already RSVP'ed RSVP again
			$canRSVP = true;
			if ($email) {
				foreach ($guests as $guest) {
					if ($guest->email == $email) {
						$canRSVP = false;
					}
				}
			}

			$attending = [];
			$notAttending = [];
			foreach ($guests as $guest) {
				if ($guest->attending) {
					$attending[] = $guest;
				} else {
					$notAttending[] = $guest;
				}
			}

			// Format Dates
			$starts = new \DateTime($tally->starts);
			$ends = new \DateTime($tally->ends);

			if ($starts->format('Mjy') == $ends->format('Mjy')) {
				$dateString = $starts->format('M. j, Y') . ' from ' . $starts->format('g:ia') . '-' . $ends->format('g:ia');
			} else {
				$dateString = $starts->format('M j, Y g:ia') . ' - ' . $ends->format('M j, Y g:ia');
			}

			return view('event', [
				'event' => $tally,
				'attending' => $attending,
				'notAttending' => $notAttending,
				'canRSVP' => $canRSVP,
				'email' => $email,
				'dateString' => $dateString,
			]);

		} catch (\Exception $e) {
			return view('errors.404', array(
				'message' => $e->getMessage(),
			));
		}
	}

	// Change to require email address
	public function rsvp($id, $token, $choice, $email = null) {
		try {

			// Load the tally
			$tally = Tally::findOrFail($id);
			if ($tally->token != $token) {
				throw new \Exception('Token mismatch');
			}

			if (Attendee::where('tally_id', $id)->where('email', $email)->count() > 0) {
				throw new \Exception('You have already RSVPed to this event');
			}

			// Save rsvp
			$attendee = new Attendee;
			$attendee->email = $email;
			$attendee->tally_id = $id;
			$attendee->attending = $choice;
			$attendee->save();

			return redirect()->action('TallyController@eventLanding', [$id, $token, $email]);

		} catch (\Exception $e) {
			return view('errors.404', array(
				'message' => $e->getMessage(),
			));
		}
	}


}
