// Change Angular to use <% %> instead of {{ }}
// so that Laravel's blade can use {{ }}
var app = angular.module('tally', ['ui.bootstrap', 'ui.bootstrap.datetimepicker'], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
}).constant('API_URL', '/api/v1/');

// Highlight inputs that have the 'select-on-click' attribute
app.directive('selectOnClick', ['$window', function ($window) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			element.on('click', function () {
				if (!$window.getSelection().toString()) {
					// Required for mobile Safari
					this.setSelectionRange(0, this.value.length)
				}
			});
		}
	};
}]);
