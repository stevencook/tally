<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendee extends Model
{
	// The attendee belongs to only one tally
	public function tally() {
		return $this->belongsTo('App\Tally');
	}
}
